# Stock prediction system

#### Description
The front and back end system built by vue3 and spring boot is adopted. python is used to write scripts. The least square method of linear regression and random forest are adopted to realize the effect of stock prediction. The front end uses echarts to draw charts; The algorithm uses the built-in model of sklearn library


#### Software Architecture

1. MySQL数据库
2. Sprint boot
3. Vue3、element-plus
4. echarts
5. python3.8


#### Installation

1. Java maven environment and vue Vue@Cli environment need to be set up for installation 
2. The system that executes python scripts through flask 2.0 needs to run app.py in it and turn on the flask application to provide services. Then, the password and port of MySQL at the back end needs to be set to your own. The stock data imported by the system is installed in the D:/stock/data directory by default, which can be set using the application.properties configuration file


#### Instructions

1. The system temporarily requires login before you can use the function of the front-end page, which is set in the back-end interceptor. The initial user is admin in the database
2. Stock data is downloaded by Tongtonxin Software


#### Contribution

1. BBYH is created at 2023-04-28. If you have any questions, please contact BBYH at 3070492097@qq.com


#### Demo screenshot

![System home page](https://gitee.com/anxwefndu/stock-prediction-system/raw/master/assert/1.png)

![System home page stock data forecast situation](https://gitee.com/anxwefndu/stock-prediction-system/raw/master/assert/2.png)

![Stock data management](https://gitee.com/anxwefndu/stock-prediction-system/raw/master/assert/3.png)

![user management](https://gitee.com/anxwefndu/stock-prediction-system/raw/master/assert/4.png)

![The least square method predicts the situation](https://gitee.com/anxwefndu/stock-prediction-system/raw/master/assert/5.png)

