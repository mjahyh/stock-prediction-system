# 股票预测系统

#### 介绍
采用vue3与spring boot搭建的前后端系统，采用python进行脚本编写，采用线性回归的最小二乘法与随机森林实现股票预测的效果；前端采用echarts绘制图表；算法采用的是sklearn库内置的模型

#### 软件架构

1. MySQL数据库
2. Sprint boot
3. Vue3、element-plus
4. echarts
5. python3.8


#### 安装教程

1. 安装需要搭建好Java的maven环境和vue的Vue@Cli环境 
2. 通过flask调用执行python脚本的系统，需要运行里面的app.py，打开flask应用提供服务，然后对于后端MySQL的密码和端口，需要设置为自己的；系统导入的股票数据会默认安装在D:/stock/data目录，可以通过application.properties配置文件设置


#### 使用说明

1. 系统暂时是要求登陆后才可以使用前端页面的功能，这个是在后端拦截器里面设置了的，初始用户在数据库里面，为admin
2. 股票数据由通达信软件下载


#### 参与贡献

1. BBYH创建2023-04-28，有问题请联系BBYH：3070492097@qq.com


#### 演示效果

![系统首页](https://gitee.com/anxwefndu/stock-prediction-system/raw/master/assert/1.png)

![系统首页股票数据预测情况](https://gitee.com/anxwefndu/stock-prediction-system/raw/master/assert/2.png)

![股票数据管理](https://gitee.com/anxwefndu/stock-prediction-system/raw/master/assert/3.png)

![用户管理](https://gitee.com/anxwefndu/stock-prediction-system/raw/master/assert/4.png)

![最小二乘法预测情况](https://gitee.com/anxwefndu/stock-prediction-system/raw/master/assert/5.png)

